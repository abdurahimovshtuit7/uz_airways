import 'package:flutter/material.dart';
import 'package:uz_airways/constants/colors.dart';

class Button extends StatelessWidget {
  final String text;
  final Function onPressedAction;
  final bool full;

  Button(this.text, this.onPressedAction, this.full);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
          horizontal: full ? 16 : MediaQuery.of(context).size.width * 0.1,
          vertical: 25),
      child: ElevatedButton(
        onPressed: () => onPressedAction(),
        child: Text(text),
        style: ElevatedButton.styleFrom(
          primary: AppColor.green,
          padding: const EdgeInsets.symmetric(vertical: 15),
          textStyle: const TextStyle(fontSize: 14, fontFamily: 'Roboto-Medium'),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6), // <-- Radius
          ),
        ),
      ),
    );
  }
}
