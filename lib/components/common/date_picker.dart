import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';

class DatePicker extends StatelessWidget {
  final String flyTime;
  final String returnTime;
  final Function presentDatePicker;

  DatePicker(this.flyTime, this.returnTime, this.presentDatePicker);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: InkWell(
              onTap: () => presentDatePicker('0'),
              child: Container(
                // width: MediaQuery.of(context).size.width * 0.43,
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: AppColor.white,
                    border: Border.all(color: AppColor.borderColor)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    flyTime.isEmpty ? Text("Туда") : Text(flyTime),
                    SvgPicture.asset(Images.calendar)
                  ],
                ),
              ),
            ),
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: InkWell(
              onTap: () => presentDatePicker('1'),
              child: Container(
                padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(6),
                    color: AppColor.white,
                    border: Border.all(color: AppColor.borderColor)),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    returnTime.isEmpty ? Text("Обратно") : Text(returnTime),
                    SvgPicture.asset(Images.calendar)
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
