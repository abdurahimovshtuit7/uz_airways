import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:select_dialog/select_dialog.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';
import 'package:uz_airways/models/flight_routes_model.dart';
import 'package:uz_airways/providers/flight_routes_provider.dart';
import 'package:uz_airways/utils/shared_pref.dart';

class Direction extends StatelessWidget {
  final String ex1;
  final String ex2;
  final Function onChange1;
  final Function onChange2;
  // final List<Datum> data;

  Direction(this.ex1, this.ex2, this.onChange1, this.onChange2);

  itemList(data) {
    return List.generate(data.length, (index) => data[index].name.toString()
        // Datum(
        //   id: data[index].id,
        //   name: "Deivão $index",
        //   // id: "$index",
        //   // createdAt: DateTime.now(),
        // ),
        );
  }

  Widget buttonInkWell(
    BuildContext context,
    String id,
    String label,
    String selected,
    dynamic onChange,
    List<Datum> data,
  ) {
    return InkWell(
      onTap: () {
        SelectDialog.showModal<String>(
          context,
          label: label,
          selectedValue: selected,
          alwaysShowScrollBar: false,
          items: itemList(data),
          searchBoxDecoration: InputDecoration(hintText: "Search"),
          onChange: (String selected) {
            onChange(selected);
          },
        );
      },
      child: selected.isEmpty
          ? Text("data")
          : Container(
              padding: const EdgeInsets.only(top: 15.0, bottom: 15),
              child: Row(
                children: [
                  Align(
                      alignment: Alignment.centerLeft,
                      child: SvgPicture.asset(
                        Images.location,
                        color: id == '1' ? AppColor.green : null,
                      )),
                  Container(
                      margin: const EdgeInsets.only(left: 10.0),
                      child: Text(
                        selected,
                        style: TextStyle(fontSize: 14.0),
                      ))
                ],
              ),
            ),
    );
  }

  @override
  Widget build(BuildContext context) {
    
    final data = Provider.of<FlightRoutesProvider>(context);
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 19),
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        border: Border.all(color: AppColor.borderColor),
        color: AppColor.white,
      ),
      child: Column(
        children: [
          buttonInkWell(context, '0', 'Countries', ex1, onChange1, data.data.data),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const Expanded(
                  child: Divider(
                color: AppColor.borderColor,
                thickness: 1,
              )),
              const SizedBox(width: 16),
              SvgPicture.asset(
                Images.upDownArrow,
                height: 15,
                width: 15,
              )
            ],
          ),
          buttonInkWell(context, '1', 'Countries', ex2, onChange2, data.data.data)
        ],
      ),
    );
  }
}
