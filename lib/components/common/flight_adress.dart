import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';

class FlightAdress extends StatelessWidget {
  const FlightAdress({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        decoration: BoxDecoration(
            color: AppColor.white,
            boxShadow: const [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(1.0, 1.0), //(x,y)
                blurRadius: 1.0,
              ),
            ],
            borderRadius: BorderRadius.circular(6)),
        child: Row(
          children: [
            SvgPicture.asset(Images.airplaneUnderline),
            Container(
              padding: EdgeInsets.only(left: 16),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Ташкент',
                    style: TextStyle(
                        fontFamily: AppFont.robotoMedium,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 8),
                  Text(
                    'TAS',
                    style: TextStyle(
                        fontFamily: AppFont.robotoRegular,
                        color: AppColor.abbreviationText.withOpacity(0.4)),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    height: 1.5,
                    width: 20,
                    color: AppColor.primary,
                  ),
                  Text(
                    'Москва',
                    style: TextStyle(
                        fontFamily: AppFont.robotoMedium,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 8),
                  Text(
                    'MOW',
                    style: TextStyle(
                        fontFamily: AppFont.robotoRegular,
                        color: AppColor.abbreviationText.withOpacity(0.4)),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
