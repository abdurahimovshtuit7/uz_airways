import 'package:flutter/material.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';

class FlightPrice extends StatelessWidget {
  const FlightPrice({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: AppColor.borderColor, width: 1),
          borderRadius: BorderRadius.circular(6),
        ),
        child: Container(
          // width: 140,
          // height: 70,
          padding: EdgeInsets.all(15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 10),
                child: Text('вт, 04 май',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                        fontFamily: AppFont.robotoRegular,
                        fontSize: 14,
                        color: AppColor.lightTextColor)),
              ),
              Text(
                '100 550 000 UZS',
                overflow: TextOverflow.ellipsis,
                maxLines: 1,
                style: TextStyle(
                    fontFamily: AppFont.robotoMedium,
                    fontSize: 14,
                    color: AppColor.segmentTextColor,
                    fontWeight: FontWeight.w500),
              )
            ],
          ),
        ),
      ),
    );
  }
}
