import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';

class FullFlightsItem extends StatelessWidget {
  const FullFlightsItem({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
        decoration: BoxDecoration(
            color: AppColor.white,
            boxShadow: const [
              BoxShadow(
                color: Colors.grey,
                offset: Offset(0.0, 1.0), //(x,y)
                blurRadius: 1.0,
              ),
            ],
            borderRadius: BorderRadius.circular(6)),
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 8.0),
              child: Row(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  SvgPicture.asset(Images.airplaneUnderline),
                  SizedBox(width: 16),
                  Text(
                    'Ташкент',
                    style: TextStyle(
                        fontFamily: AppFont.robotoMedium,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 8),
                  Text(
                    'TAS',
                    style: TextStyle(
                        fontFamily: AppFont.robotoRegular,
                        color: AppColor.abbreviationText.withOpacity(0.4)),
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 8),
                    height: 1.5,
                    width: 20,
                    color: AppColor.primary,
                  ),
                  SvgPicture.asset(Images.airplaneUnderline),
                  SizedBox(width: 16),
                  Text(
                    'Москва',
                    style: TextStyle(
                        fontFamily: AppFont.robotoMedium,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(width: 8),
                  Text(
                    'MOW',
                    style: TextStyle(
                        fontFamily: AppFont.robotoRegular,
                        color: AppColor.abbreviationText.withOpacity(0.4)),
                  ),
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: Text(
                          '20:00',
                          style: TextStyle(
                              color: AppColor.primary,
                              fontFamily: AppFont.robotoMedium,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Text(
                        'Ташкент',
                        style: TextStyle(
                            color: AppColor.lightTextColor,
                            fontFamily: AppFont.robotoRegular,
                            fontSize: 12),
                      ),
                    ],
                  ),
                  Container(
                    child: Text(
                      'HY 017 (Airbus 320)',
                      style: TextStyle(
                          color: AppColor.primary,
                          fontFamily: AppFont.robotoMedium,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(bottom: 5),
                        child: Text(
                          '05:20',
                          style: TextStyle(
                              color: AppColor.primary,
                              fontFamily: AppFont.robotoMedium,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                      Text(
                        'Москва',
                        style: TextStyle(
                            color: AppColor.lightTextColor,
                            fontFamily: AppFont.robotoRegular,
                            fontSize: 12),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Divider(),
            Padding(
              padding: EdgeInsets.only(top: 16),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      children: [
                        Text(
                          'Время в пути:',
                          style: TextStyle(
                              color: AppColor.lightTextColor,
                              fontFamily: AppFont.robotoRegular,
                              fontSize: 12),
                        ),
                        SizedBox(width: 5),
                        Text(
                          '04:00',
                          style: TextStyle(
                              color: AppColor.primary,
                              fontFamily: AppFont.robotoRegular,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Row(
                      children: [
                        Text(
                          'Расстояние:',
                          style: TextStyle(
                              color: AppColor.lightTextColor,
                              fontFamily: AppFont.robotoRegular,
                              fontSize: 12),
                        ),
                        SizedBox(width: 5),
                        Text(
                          '3336 км',
                          style: TextStyle(
                              color: AppColor.primary,
                              fontFamily: AppFont.robotoRegular,
                              fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
