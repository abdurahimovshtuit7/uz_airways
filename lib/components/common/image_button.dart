import 'package:flutter/material.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';

class ImageButton extends StatelessWidget {
  const ImageButton({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Card(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(6),
          ),
          elevation: 2,
          color: AppColor.backgroundColor,
          margin: const EdgeInsets.symmetric(horizontal: 8),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                borderRadius: const BorderRadius.only(
                  topLeft: Radius.circular(6),
                  topRight: Radius.circular(6),
                ),
                child: Image.network(
                  'https://cabar.asia/wp-content/uploads/2020/06/uzbekistan-airways-740x480.jpg',
                  height: 110,
                  width: 280,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                // height: 104,
                padding: const EdgeInsets.all(16),
                child: Text(
                  'Завораживающий Сеул!',
                  style: TextStyle(
                    color: AppColor.segmentTextColor,
                    fontSize: 14,
                    fontFamily: AppFont.robotoMedium,
                    fontWeight: FontWeight.w500,
                  ),
                ),
              ),
              Container(
                // height: 104,
                width: 280,
                padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
                child: Text(
                  'Uzbekistan Airways объявляет о введении второй частоты по маршруту Каракалпакстан - ',
                  maxLines: 2,
                  // textAlign: TextAlign.end,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: AppColor.descriptionText,
                    fontSize: 12,
                    fontFamily: AppFont.robotoRegular,
                  ),
                ),
              ),
            ],
          )),
    );
  }
}
