import 'package:flutter/material.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';

class InputField extends StatelessWidget {
  final String title;
  final String label;

  InputField(this.title, this.label);

  @override
  Widget build(BuildContext context) {
    return Column(crossAxisAlignment: CrossAxisAlignment.stretch, children: [
      Container(
        margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1,
        ),
        child: Text(
          title,
          style: TextStyle(
            color: AppColor.white,
            fontFamily: AppFont.robotoRegular,
            fontSize: 14,
          ),
          // textAlign: TextAlign.start,
        ),
      ),
      Container(
        margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.1,
          vertical: 13,
        ),
        child: TextFormField(
          decoration: InputDecoration(
            hintText: label,
            hintStyle: TextStyle(fontSize: 14, fontFamily: AppFont.robotoRegular),
            fillColor: AppColor.white,
            filled: true,
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(6.0)),
              borderSide: BorderSide(color: Colors.orange),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(6.0)),
              borderSide: BorderSide(color: Colors.white),
            ),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(6.0)),
            ),
          ),
          style: TextStyle(fontSize: 14, fontFamily: AppFont.robotoRegular),
          cursorColor: AppColor.primary,
          validator: (String? value) {
            if (value == null || value.isEmpty) {
              return 'Please enter some text';
            }
            return null;
          },
        ),
      ),
    ]);
  }
}
