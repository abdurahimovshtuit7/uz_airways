import 'package:flutter/material.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';

class UnderlineButton extends StatelessWidget {
  final String text;
  final String routeName;
  // final Function onPressAction;
  // void UnderlineButtonPressed(BuildContext context, String routeName) {
  //   Navigator.of(context).pushNamed(routeName);
  // }

  UnderlineButton(this.text, this.routeName);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: TextButton(
        child: Text(
          text,
          style: TextStyle(
            decoration: TextDecoration.underline,
            color: AppColor.white,
            fontFamily: AppFont.robotoRegular,
            fontSize: 14,
          ),
        ),
        onPressed: () => Navigator.of(context).pushNamed(routeName),
      ),
    );
  }
}
