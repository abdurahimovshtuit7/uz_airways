
class AppFont {
  AppFont._();

  static String robotoBold = 'Roboto-Bold';
  static String robotoRegular = 'Roboto-Regular';
  static String robotoMedium = 'Roboto-Medium';

}