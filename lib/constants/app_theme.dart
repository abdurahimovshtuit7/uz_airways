import 'package:flutter/material.dart';

import 'app_font.dart';
import 'colors.dart';

final ThemeData themeData = ThemeData(
    // primarySwatch: Colors,
    fontFamily: AppFont.robotoRegular,
    primaryColor: AppColor.primary,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: AppBarTheme(
      backgroundColor: AppColor.white,
      iconTheme: IconThemeData(
        color: AppColor.primary, //change your color here
      ),
    ));
