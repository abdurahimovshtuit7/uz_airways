import 'package:flutter/material.dart';

class AppColor {
  AppColor._();

  static const Color primary = Color(0xFF0D2961);
  static const Color green = Color(0xFF478C3E);
  static const Color backgroundColor = Color(0xFFF6F7FB);
  static const Color segmentedControl = Color(0xFFECEFF6);
  static const Color segmentTextColor = Color(0xFF082644);
  static const Color secondary = Color(0xFFFF880E);
  static const Color secondaryVariant = Color(0xFFF28665);
  static const Color borderColor = Color(0xFFDCE3EB);
  static const Color abbreviationText = Color(0xFF0F477F);
  static const Color descriptionText = Color(0xFF7B7E8E);
  static const Color lightTextColor = Color(0xFF797F9A);

  // main text colors
  static const Color slightGrey = Color(0xFFE1E1E1);
  static const Color coolGrey = Color(0xFFBFC6D5);
  static const Color greyishBrown = Color(0xFF4A4A4A);
  static const Color darkWhite = Color(0xFFF5F5F5);
  static const Color white = Color(0xFFFFFFFF);
  static const Color warmGrey = Color(0xFF979797);
}
