class Images {
  Images._();

  //from icons directory
  static const String appLogo = 'assets/icons/app_logo.svg';
  static const String facebook = 'assets/icons/facebook.svg';
  static const String google = 'assets/icons/google.svg';
  static const String wiki = 'assets/icons/wiki.svg';
  static const String appLogoBlue = 'assets/icons/app_logo_blue.svg';
  static const String airPlane = 'assets/icons/air_plane.svg';
  static const String ticket = 'assets/icons/ticket.svg';
  static const String profileIcon = 'assets/icons/profile_icon.svg';
  static const String upDownArrow = 'assets/icons/up_down_arrow.svg';
  static const String location = 'assets/icons/location.svg';
  static const String calendar = 'assets/icons/calendar.svg';
  static const String account = 'assets/icons/account.svg';
  static const String airplaneUnderline = 'assets/icons/airplane_underline.svg';

  //from images directory
  static const String background = 'assets/images/background.jpg';
}
