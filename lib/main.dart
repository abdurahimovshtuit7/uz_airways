import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/providers/flight_routes_provider.dart';
import 'package:uz_airways/routes.dart';
import 'package:uz_airways/translations/codegen_loader.g.dart';
// import 'dart:io' show Platform;

// import './pages/auth/login/login_page.dart';
import 'constants/app_theme.dart';
import 'package:easy_localization/easy_localization.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  await EasyLocalization.ensureInitialized();
  // bool platform = UniversalPlatform.isIOS;
  SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
    statusBarColor: AppColor.primary, // transparent status bar
    // statusBarIconBrightness: ? Brightness.dark:Brightness.light // dark text for status bar
  ));
  runApp(
    EasyLocalization(
        supportedLocales: const [
          Locale('en'),
          Locale('ru'),
          Locale('uz'),
        ],
        path:
            'assets/translations', // <-- change the path of the translation files
        fallbackLocale: const Locale('ru'),
        startLocale: const Locale('uz'),
        assetLoader: const CodegenLoader(),
        useOnlyLangCode: true,
        child: MyApp()),
  );
}

class MyApp extends StatelessWidget {
  // const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // FlutterStatusbarcolor.setStatusBarColor(Colors.white);
    // ignore: avoid_print
    print(context.fallbackLocale); // output: en_US

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (ctx) => FlightRoutesProvider()),
      ],
      child: MaterialApp(
        localizationsDelegates: context.localizationDelegates,
        supportedLocales: context.supportedLocales,
        locale: context.locale,
        debugShowCheckedModeBanner: false,
        // title: 'Flutter Demo',
        theme: themeData,
        initialRoute: Routes.tabs,
        // home: Text(Routes.tabs),
        routes: Routes.routes,
      ),
    );
  }
}
