// To parse this JSON data, do
//
//     final flightRoutes = flightRoutesFromJson(jsonString);

import 'dart:convert';

FlightRoutes flightRoutesFromJson(String str) =>
    FlightRoutes.fromJson(json.decode(str));

String flightRoutesToJson(FlightRoutes data) => json.encode(data.toJson());

class FlightRoutes {
  final bool success;
  List<Datum> data;
  final dynamic links;
  final dynamic errors;

  FlightRoutes({
    required this.success,
    required this.data,
    this.links,
    this.errors,
  });

  factory FlightRoutes.fromJson(Map<String, dynamic> json) => FlightRoutes(
        success: json["success"],
        data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
        links: json["links"],
        errors: json["errors"],
      );

  get isEmpty => null;

  Map<String, dynamic> toJson() => {
        "success": success,
        "data": List<dynamic>.from(data.map((x) => x.toJson())),
        "links": links,
        "errors": errors,
      };
}

class Datum {
  int id;
  String name;
  String code;
  bool international;
  bool roundTripRequired;

  Datum({
    required this.id,
    required this.name,
    required this.code,
    required this.international,
    required this.roundTripRequired,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
        id: json["id"],
        name: json["name"],
        code: json["code"],
        international: json["international"],
        roundTripRequired: json["round_trip_required"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "code": code,
        "international": international,
        "round_trip_required": roundTripRequired,
      };
}
