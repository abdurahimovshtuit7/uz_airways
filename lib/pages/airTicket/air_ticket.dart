// ignore: file_names
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uz_airways/models/flight_routes_model.dart';
import 'package:uz_airways/providers/flight_routes_provider.dart';
import 'package:uz_airways/utils/shared_pref.dart';

import '../../components/common/button.dart';
import '../../components/common/date_picker.dart';
import '../../components/common/direction.dart';
import '../../components/common/flight_adress.dart';
import '../../components/common/image_button.dart';
import '../../constants/app_font.dart';
import '../../constants/colors.dart';
import '../../constants/images.dart';
import 'package:uz_airways/routes.dart';
import 'passengers_number.dart';

class AirTicketScreen extends StatefulWidget {
  @override
  State<AirTicketScreen> createState() => _AirTicketScreenState();
}

class _AirTicketScreenState extends State<AirTicketScreen> {
  bool isChecked = false;
  String ex1 = 'Откуда';
  String ex2 = 'Куда';
  int passangersNumber = 1;
  String flyTime = '';
  String returnTime = '';

  int? _segmentedControlGroupValue = 0;

  final Map<int, Widget> myTabs = <int, Widget>{
    0: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          "Прямой рейс",
          style: TextStyle(
              fontFamily: AppFont.robotoMedium,
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: AppColor.segmentTextColor),
        )),
    1: Padding(
        padding: EdgeInsets.symmetric(vertical: 10),
        child: Text(
          "Составной маршрут",
          style: TextStyle(
              fontFamily: AppFont.robotoMedium,
              fontSize: 14,
              fontWeight: FontWeight.w500,
              color: AppColor.segmentTextColor),
        ))
  };

  void onChange1(selected) {
    setState(() {
      ex1 = selected;
    });
  }

  void onChange2(selected) {
    setState(() {
      ex2 = selected;
    });
  }

  void _presentDatePicker(String id) {
    showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime.now(),
      lastDate: DateTime(2022),
    ).then((pickedDate) {
      if (id == '0') {
        setState(() {
          flyTime = DateFormat.yMMMd().format(pickedDate!);
        });
      } else {
        setState(() {
          returnTime = DateFormat.yMMMd().format(pickedDate!);
        });
      }
    });
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();
    final data = Provider.of<FlightRoutesProvider>(context, listen: true);

    Response response = await Dio().get(
      'https://apihavo.igenius.uz/api/v1/mobile/uzairways/routes',
    );
    if (response.statusCode == 200) {
      // setState(() {
      //   resData = FlightRoutes.fromJson(response.data);
      // });

      FlightRoutes responseData = FlightRoutes.fromJson(response.data);
      data.changeRoutesData(responseData);
    } else {
      throw Exception('Failed to load album');
    }
  }

  // void bottomSheet(BuildContext ctx) {
  //   showModalBottomSheet(
  //       context: ctx,
  //       builder: (bCtx) {
  //         return GestureDetector(
  //           onTap: () {},
  //           child: Container(
  //             child: Text('1232'),
  //           ),
  //           behavior: HitTestBehavior.opaque,
  //         );
  //       });
  // }

  //popUp screen
  Route _createRoute() {
    return PageRouteBuilder(
      pageBuilder: (context, animation, secondaryAnimation) =>
          const PassengersNumber(),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = const Offset(0.0, 1.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween =
            Tween(begin: begin, end: end).chain(CurveTween(curve: curve));

        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  //text Widget
  Widget _textWidget(String title, double Size) {
    var size = Size;
    return Container(
      margin: EdgeInsets.only(left: 16, top: 17),
      child: Text(
        'Авиабилеты',
        style: TextStyle(
            fontFamily: AppFont.robotoBold,
            fontSize: size,
            fontWeight: FontWeight.bold),
      ),
    );
  }

  void onPress() async{
    SharedPref sharedPref = SharedPref();

    // await sharedPref.save('flightRoutes', 'Shaxboz');
    final text =  await sharedPref.read("flightRoutes");
    // ignore: avoid_print
    print(text.toString());

    // Navigator.of(context).pushNamed(Routes.flightList);
  }

  @override
  Widget build(BuildContext context) {
    SharedPref sharedPref = SharedPref();
    
    final data = Provider.of<FlightRoutesProvider>(context);

    Color getColor(Set<MaterialState> states) {
      const Set<MaterialState> interactiveStates = <MaterialState>{
        MaterialState.pressed,
        MaterialState.hovered,
        MaterialState.focused,
      };
      if (states.any(interactiveStates.contains)) {
        return Colors.blue;
      }
      return AppColor.green;
    }

    return Scaffold(
      body: SafeArea(
          child: Stack(children: [
        Container(
          color: AppColor.backgroundColor,
        ),
        SingleChildScrollView(
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Center(
                child: Container(
                  width: double.infinity,
                  height: 70,
                  padding: const EdgeInsets.symmetric(vertical: 20),
                  decoration: const BoxDecoration(
                    color: AppColor.white,
                    border: Border(
                      bottom: BorderSide(color: AppColor.borderColor, width: 1),
                    ),
                  ),
                  child: SvgPicture.asset(
                    Images.appLogoBlue,
                    height: 50,
                    width: 80,
                  ),
                ),
              ),

              //TextWidget
              _textWidget('Авиабилеты', 18),
              Container(
                width: double.infinity,
                padding:
                    const EdgeInsets.symmetric(vertical: 13, horizontal: 15),
                child: CupertinoSlidingSegmentedControl(
                    thumbColor: AppColor.white,
                    groupValue: _segmentedControlGroupValue,
                    children: myTabs,
                    backgroundColor: AppColor.segmentedControl,
                    padding: EdgeInsets.all(5),
                    onValueChanged: (int? value) {
                      setState(() {
                        _segmentedControlGroupValue = value;
                      });
                    }),
              ),

              _textWidget('Направление', 16),

              Direction(ex1, ex2, onChange1, onChange2),
              //Checkbox
              Row(
                children: [
                  Checkbox(
                    checkColor: Colors.white,
                    fillColor: MaterialStateProperty.resolveWith(getColor),
                    value: isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        isChecked = value!;
                      });
                    },
                  ),
                  Text(
                    'Туда и обратно',
                    style: TextStyle(
                      fontFamily: AppFont.robotoMedium,
                      fontWeight: FontWeight.w500,
                      fontSize: 16,
                    ),
                  ),
                  Checkbox(
                    checkColor: Colors.white,
                    fillColor: MaterialStateProperty.resolveWith(getColor),
                    value: !isChecked,
                    onChanged: (bool? value) {
                      setState(() {
                        if (value == isChecked) {
                          isChecked = !isChecked;
                        } else
                          isChecked = isChecked;
                      });
                    },
                  ),
                ],
              ),
              //DatePicker
              DatePicker(flyTime, returnTime, _presentDatePicker),
              //Component
              InkWell(
                onTap: () {
                  Navigator.of(context).push(_createRoute()).then((result) {
                    setState(() {
                      passangersNumber = result;
                    });
                  });
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                  padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: AppColor.white,
                      border: Border.all(color: AppColor.borderColor)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Text("$passangersNumber пассажира"),
                      SvgPicture.asset(Images.account)
                    ],
                  ),
                ),
              ),

              Button('Искать', onPress, true),

              _textWidget('Недавно посмотренные', 16),

              // for (int i = 0; i <= 5;i++){
              //   return   FlightAdress();
              // },
              Column(
                children: <Widget>[
                  ListView.builder(
                    itemBuilder: (BuildContext context, int index) =>
                        FlightAdress(),
                    itemCount: 3,
                    physics: NeverScrollableScrollPhysics(),
                    shrinkWrap: true, // use it
                  )
                ],
              ),

              _textWidget('Новости', 16),

              //Horizontal list
              SizedBox(
                height: 245,
                child: ListView.builder(
                    padding: const EdgeInsets.symmetric(
                        vertical: 15.0, horizontal: 8),
                    scrollDirection: Axis.horizontal,
                    itemCount: 300,
                    itemBuilder: (BuildContext context, int index) =>
                        ImageButton()),
              ),
            ],
          ),
        ),
      ])),
    );
  }
}
