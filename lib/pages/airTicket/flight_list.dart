import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/components/common/button.dart';
import 'package:uz_airways/components/common/flight_price.dart';
import 'package:uz_airways/components/common/full_flights_item.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';

class FlightList extends StatelessWidget {
  static const String routeName = 'flight-list';
  const FlightList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColor.backgroundColor,
      appBar: AppBar(
        elevation: 1,
        brightness: Brightness.dark,
        title: Stack(
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  'MOW - TAS',
                  style: TextStyle(
                      fontSize: 16.0,
                      color: AppColor.primary,
                      fontFamily: AppFont.robotoMedium,
                      fontWeight: FontWeight.w500),
                ),
                Text(
                  '04 май - 06 май | 1 взрослый',
                  style: TextStyle(
                      fontSize: 12.0,
                      color: AppColor.lightTextColor,
                      fontFamily: AppFont.robotoMedium,
                      fontWeight: FontWeight.w500),
                ),
              ],
            ),
          ],
        ),
        iconTheme: IconThemeData(
          color: AppColor.primary, //change your color here
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              // width: double.infinity,
              height: 120,
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: ListView.builder(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                scrollDirection: Axis.horizontal,
                itemBuilder: (ctx, index) {
                  return FlightPrice();
                },
                itemCount: 8,
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 16, top: 17),
              child: Text(
                'Найденные рейсы',
                style: TextStyle(
                    fontFamily: AppFont.robotoBold,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),
            //Component
            FullFlightsItem(),
            Container(
              margin: EdgeInsets.only(left: 16, top: 17),
              child: Text(
                'Тарифы',
                style: TextStyle(
                    fontFamily: AppFont.robotoBold,
                    fontSize: 16,
                    fontWeight: FontWeight.w500),
              ),
            ),

            //Component
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              padding: const EdgeInsets.only(
                  bottom: 0, top: 16, left: 16, right: 16),
              decoration: BoxDecoration(
                  color: AppColor.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 1.0,
                    ),
                  ],
                  borderRadius: BorderRadius.circular(6)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 11),
                    child: Text(
                      'Бизнес',
                      style: TextStyle(
                          fontFamily: AppFont.robotoBold,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'Цена',
                              style: TextStyle(
                                  fontFamily: AppFont.robotoRegular,
                                  fontSize: 12,
                                  color: AppColor.lightTextColor),
                            ),
                          ),
                          Text(
                            '1 492 963 UZS',
                            style: TextStyle(
                                fontFamily: AppFont.robotoBold,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                      Container(
                          height: 30,
                          child: VerticalDivider(color: AppColor.warmGrey)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'Багаж',
                              style: TextStyle(
                                  fontFamily: AppFont.robotoRegular,
                                  fontSize: 12,
                                  color: AppColor.lightTextColor),
                            ),
                          ),
                          Text(
                            'Одно место - 32 кг',
                            style: TextStyle(
                                fontFamily: AppFont.robotoBold,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    ],
                  ),
                  Button('Выбрать', () {}, true),
                ],
              ),
            ),
            //Component
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              padding: const EdgeInsets.only(
                  bottom: 0, top: 16, left: 16, right: 16),
              decoration: BoxDecoration(
                  color: AppColor.white,
                  boxShadow: const [
                    BoxShadow(
                      color: Colors.grey,
                      offset: Offset(0.0, 1.0), //(x,y)
                      blurRadius: 1.0,
                    ),
                  ],
                  borderRadius: BorderRadius.circular(6)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(bottom: 11),
                    child: Text(
                      'Экономический',
                      style: TextStyle(
                          fontFamily: AppFont.robotoBold,
                          fontSize: 16,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'Цена',
                              style: TextStyle(
                                  fontFamily: AppFont.robotoRegular,
                                  fontSize: 12,
                                  color: AppColor.lightTextColor),
                            ),
                          ),
                          Text(
                            '816 648 UZS',
                            style: TextStyle(
                                fontFamily: AppFont.robotoBold,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                      Container(
                          height: 30,
                          child: VerticalDivider(color: AppColor.warmGrey)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 5),
                            child: Text(
                              'Багаж',
                              style: TextStyle(
                                  fontFamily: AppFont.robotoRegular,
                                  fontSize: 12,
                                  color: AppColor.lightTextColor),
                            ),
                          ),
                          Text(
                            'Одно место - 32 кг',
                            style: TextStyle(
                                fontFamily: AppFont.robotoBold,
                                fontSize: 14,
                                fontWeight: FontWeight.w500),
                          )
                        ],
                      ),
                    ],
                  ),
                  Button('Выбрать', () {}, true),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
