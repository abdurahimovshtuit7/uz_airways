import 'package:flutter/material.dart';
import 'package:uz_airways/components/common/button.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';

class PassengersNumber extends StatefulWidget {
  const PassengersNumber({Key? key}) : super(key: key);

  @override
  State<PassengersNumber> createState() => _PassengersNumberState();
}

class _PassengersNumberState extends State<PassengersNumber> {
  int adultsNumber = 1;
  int childrenNumber = 0;
  int babiesNumber = 0;

  void add(int adultsNumber, int childrenNumber, int babiesNumber) {
    int sum = adultsNumber + childrenNumber + babiesNumber;
    Navigator.of(context).pop(sum);
  }

  Widget passangersCounter(String title, String description, int number,
      dynamic add, dynamic remove) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 16),
      child: Row(
        children: [
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                child: Text(
                  title,
                  style: TextStyle(
                      color: AppColor.segmentTextColor,
                      fontFamily: AppFont.robotoMedium,
                      fontWeight: FontWeight.w500,
                      fontSize: 14),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width * 0.5,
                child: Text(
                  description,
                  style: TextStyle(
                      color: AppColor.lightTextColor,
                      fontFamily: AppFont.robotoRegular,
                      fontSize: 14),
                ),
              ),
            ],
          ),
          Expanded(child: Container()),
          IconButton(
              onPressed: remove,
              highlightColor: number == 0 ? Colors.transparent : null,
              // hoverColor: Colors.transparent,
              splashColor: Colors.transparent,
              icon: Icon(
                Icons.remove_circle_outline,
                size: 32,
                color: number == 0
                    ? AppColor.green.withOpacity(0.3)
                    : AppColor.green,
              )),
          Container(
            child: Text(
              number.toString(),
              style: TextStyle(
                  color: AppColor.segmentTextColor,
                  fontFamily: AppFont.robotoRegular,
                  fontSize: 24),
            ),
          ),
          IconButton(
            onPressed: add,
            icon: Icon(
              Icons.add_circle_outline,
              size: 32,
              color: AppColor.green,
            ),
          ),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          brightness: Brightness.dark,
          title: Text(
            'Количество пассажиров',
            style: TextStyle(color: AppColor.segmentTextColor),
          ),
          elevation: 1,
          // leading: null,
          automaticallyImplyLeading: false, // Used for removing back buttoon.
          actions: [
            IconButton(
                onPressed: () => Navigator.of(context).pop(),
                icon: Icon(
                  Icons.close_rounded,
                  color: AppColor.segmentTextColor,
                ))
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                padding: EdgeInsets.all(16),
                child: Text(
                  'Выбирайте возраст пассажира на момент перелета',
                  style: TextStyle(
                      fontFamily: AppFont.robotoRegular,
                      fontSize: 14,
                      color: AppColor.lightTextColor),
                ),
              ),
              passangersCounter(
                'Взрослые',
                'от 12 лет',
                adultsNumber,
                () => setState(() {
                  adultsNumber++;
                }),
                () {
                  if (adultsNumber != 0) {
                    setState(() {
                      adultsNumber--;
                    });
                  }
                },
              ),
              Divider(),
              passangersCounter(
                'Дети',
                'от 0 до 12 лет',
                childrenNumber,
                () => setState(() {
                  childrenNumber++;
                }),
                () {
                  if (childrenNumber != 0) {
                    setState(() {
                      childrenNumber--;
                    });
                  }
                },
              ),
              Divider(),
              passangersCounter(
                'Младенцы',
                'до 2 лет, без места на руках у взрослого',
                babiesNumber,
                () => setState(() {
                  babiesNumber++;
                }),
                () {
                  if (babiesNumber != 0) {
                    setState(() {
                      babiesNumber--;
                    });
                  }
                },
              ),
              Button('Сохранить',
                  () => add(adultsNumber, childrenNumber, babiesNumber), true)
            ],
          ),
        ));
  }
}
