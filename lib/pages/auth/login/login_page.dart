import 'dart:convert';
import 'dart:ui';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';
import 'package:uz_airways/components/common/button.dart';
import 'package:uz_airways/components/common/input_field.dart';
import 'package:uz_airways/components/common/underline_button.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';
import 'package:uz_airways/models/flight_routes_model.dart';
import 'package:uz_airways/providers/flight_routes_provider.dart';
import 'package:uz_airways/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';

import '../../../routes.dart';

class LoginPage extends StatefulWidget {
  static const routeName = 'sign-in';

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  var dio = Dio();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  FlightRoutes? resData;

  void onPress() {
    if (_formKey.currentState!.validate()) {
      print('Nice');
    } else {
      print('Something error');
    }
  }

  // @override
  // void didChangeDependencies() async {
  //   super.didChangeDependencies();
  //   final data = Provider.of<FlightRoutesProvider>(context, listen: true);

  //   Response response = await dio.get(
  //     'https://apihavo.igenius.uz/api/v1/mobile/uzairways/routes',
  //   );
  //   if (response.statusCode == 200) {

  //     // setState(() {
  //     //   resData = FlightRoutes.fromJson(response.data);
  //     // });

  //     FlightRoutes responseData = FlightRoutes.fromJson(response.data);
  //     data.changeRoutesData(responseData);
  //   } else {
  
  //     throw Exception('Failed to load album');
  //   }
  // }

  // void UnderlineButtonPressed(BuildContext context, String routeName) {
  //   Navigator.of(context).pushNamed(routeName);
  // }

  @override
  Widget build(BuildContext context) {
    // ignore: avoid_print
    print(context.fallbackLocale);
    final data = Provider.of<FlightRoutesProvider>(context);
    // print('resData:${resData?.data[0].name.toString()}');

    // var logger = Logger();
    // Logger().d(data.data.data[0]);

    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: AppColor.primary,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: LayoutBuilder(builder: (context, constraint) {
          return Form(
            key: _formKey,
            child: Container(
              height: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    Images.background,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 0.0, sigmaY: 0.0),
                child: Container(
                  color: AppColor.primary.withOpacity(0.8),
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints:
                          BoxConstraints(minHeight: constraint.maxHeight),
                      child: IntrinsicHeight(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    child: Center(
                                      child: SvgPicture.asset(Images.appLogo),
                                    ),
                                    margin: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.04,
                                        bottom:
                                            MediaQuery.of(context).size.height *
                                                0.07),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 25),
                                    child: Text(
                                      LocaleKeys.enter,
                                      style: TextStyle(
                                        color: AppColor.white,
                                        fontFamily: AppFont.robotoRegular,
                                        fontSize: 24,
                                      ),
                                      textAlign: TextAlign.center,
                                    ).tr(),
                                  ),

                                  //Component for TextInput
                                  InputField(LocaleKeys.emailPhoneNumber.tr(),
                                      'Enter your email'),
                                  //Component for TextInput
                                  InputField(LocaleKeys.password.tr(),
                                      'Enter your password'),

                                  //Component for Button
                                  Button(LocaleKeys.enter.tr(), onPress, false),

                                  UnderlineButton(LocaleKeys.registorNow.tr(),
                                      Routes.signUp),
                                  UnderlineButton(
                                      LocaleKeys.forgotPassword.tr(),
                                      Routes.recoveryPass),
                                ],
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Center(
                                  child: Column(
                                    children: [
                                      Container(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 20),
                                        child: Text(
                                          'h',
                                          // (data.data.data.isEmpty)
                                          //     ? 'h'
                                          //     : data.data.data[0].name
                                          //         .toString(),
                                          style: TextStyle(
                                            color: AppColor.white,
                                            fontFamily: AppFont.robotoRegular,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          IconButton(
                                            icon: SvgPicture.asset(
                                                Images.facebook),
                                            onPressed: () {},
                                          ),
                                          SizedBox(width: 13),
                                          IconButton(
                                            icon:
                                                SvgPicture.asset(Images.google),
                                            onPressed: () {},
                                          ),
                                          SizedBox(width: 13),
                                          IconButton(
                                            icon: SvgPicture.asset(
                                              Images.wiki,
                                            ),
                                            onPressed: () {},
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
