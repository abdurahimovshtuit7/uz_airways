import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/components/common/button.dart';
import 'package:uz_airways/components/common/underline_button.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';
import 'package:flutter_verification_code/flutter_verification_code.dart';

class RecoveryPasswordPage extends StatefulWidget {
  const RecoveryPasswordPage({Key? key}) : super(key: key);
  static const routeName = 'recovery-password';

  @override
  _RecoveryPasswordPageState createState() => _RecoveryPasswordPageState();
}

class _RecoveryPasswordPageState extends State<RecoveryPasswordPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool _onEditing = true;
  late String _code;
  final interval = const Duration(seconds: 1);

  final int timerMaxSeconds = 20;

  int currentSeconds = 0;

  String get timerText =>
      '${((timerMaxSeconds - currentSeconds) ~/ 60).toString().padLeft(2, '0')} : ${((timerMaxSeconds - currentSeconds) % 60).toString().padLeft(2, '0')}';

  startTimeout([int? milliseconds]) {
    var duration = interval;
    Timer.periodic(duration, (timer) {
      setState(() {
        print(timer.tick);
        currentSeconds = timer.tick;
        if (timer.tick >= timerMaxSeconds) timer.cancel();
      });
    });
  }

  void onPress() {
    if (_formKey.currentState!.validate()) {
      print('Nice');
    } else {
      print('Something error');
    }
  }

  @override
  void initState() {
    startTimeout();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Center(child: SvgPicture.asset(Images.appLogoBlue)),
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_back,
            color: AppColor.primary,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        // backgroundColor: AppColor.white,
        // toolbarHeight: 0,
        brightness: Brightness.dark,
        elevation: 1,
      ),
      body: SafeArea(
          bottom: false,
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 15),
            child: Form(
              key: _formKey,
              child: Column(
                children: [
                  Expanded(
                    // flex: 1,
                    child: Column(
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(vertical: 24),
                          child: Text(
                            'На ваш номер телефона был отправлен СМС с кодом подтверждения',
                            textAlign: TextAlign.center,
                            style: TextStyle(fontFamily: AppFont.robotoRegular),
                          ),
                        ),
                        // Timer(duration, callback)
                        SizedBox(height: 16),
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(Icons.timer),
                            SizedBox(
                              width: 5,
                            ),
                            Text(timerText)
                          ],
                        ),

                        VerificationCode(
                          textStyle: TextStyle(
                              fontSize: 20.0, color: AppColor.primary),
                          underlineColor: AppColor.primary,
                          keyboardType: TextInputType.number,
                          length: 4,
                          onCompleted: (String value) {
                            setState(() {
                              _code = value;
                            });
                          },
                          onEditing: (bool value) {
                            setState(() {
                              _onEditing = value;
                            });
                          },
                        ),
                        timerText == '00 : 00'
                            ? Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: TextButton(
                                  onPressed: () {
                                    startTimeout();
                                  },
                                  child: Text(
                                    'Отправить повторно',
                                    style: TextStyle(
                                        decoration: TextDecoration.underline,
                                        color: AppColor.green,
                                        fontFamily: AppFont.robotoRegular),
                                  ),
                                ),
                              )
                            : Container()
                        // UnderlineButton('Отправить повторно', 'home'),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10),
                    child: Button('Подтвердить', onPress, false),
                  )
                ],
              ),
            ),
          )),
    );
  }
}
