import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:flutter/widgets.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/components/common/button.dart';
import 'package:uz_airways/components/common/input_field.dart';
import 'package:uz_airways/components/common/underline_button.dart';
import 'package:uz_airways/constants/app_font.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';
import 'package:uz_airways/translations/locale_keys.g.dart';
import 'package:easy_localization/easy_localization.dart';


import '../../../routes.dart';

// ignore: use_key_in_widget_constructors
class SignUpPage extends StatefulWidget {
  static const routeName = 'sign-up';

  // SignUpPage({Key key}) : super(key: key);

  @override
  _SignUpPageState createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  void onPress() {
    if (_formKey.currentState!.validate()) {
      print('Nice');
    } else {
      print('Something error');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 0,
        backgroundColor: AppColor.primary,
        brightness: Brightness.dark,
        elevation: 0,
      ),
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        bottom: false,
        child: LayoutBuilder(builder: (context, constraint) {
          return Form(
            key: _formKey,
            child: Container(
              height: double.infinity,
              decoration: const BoxDecoration(
                image: DecorationImage(
                  image: AssetImage(
                    Images.background,
                  ),
                  fit: BoxFit.cover,
                ),
              ),
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 0.0, sigmaY: 0.0),
                child: Container(
                  color: AppColor.primary.withOpacity(0.8),
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints:
                          BoxConstraints(minHeight: constraint.maxHeight),
                      child: IntrinsicHeight(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.stretch,
                          children: <Widget>[
                            Expanded(
                              flex: 4,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.stretch,
                                children: [
                                  Container(
                                    child: Center(
                                      child: SvgPicture.asset(Images.appLogo),
                                    ),
                                    margin: EdgeInsets.only(
                                        top:
                                            MediaQuery.of(context).size.height *
                                                0.04,
                                        bottom:
                                            MediaQuery.of(context).size.height *
                                                0.07),
                                  ),
                                  Padding(
                                    padding: EdgeInsets.only(bottom: 25),
                                    child: Text(
                                      LocaleKeys.signUp.tr(),
                                      style: TextStyle(
                                        color: AppColor.white,
                                        fontFamily: AppFont.robotoRegular,
                                        fontSize: 24,
                                      ),
                                      textAlign: TextAlign.center,
                                    ),
                                  ),

                                  //Component for TextInput

                                  InputField(LocaleKeys.emailPhoneNumber.tr(),
                                      'Enter your email'),
                                  //Component for TextInput
                                  InputField(LocaleKeys.password.tr(), 'Enter your password'),
                                  //Component for Button
                                  Button(LocaleKeys.registorNow.tr(), onPress, false),

                                  UnderlineButton(
                                      LocaleKeys.haveAccount.tr(), Routes.loginPage),
                                ],
                              ),
                            ),
                            Expanded(
                                flex: 1,
                                child: Center(
                                  child: Column(
                                    children: [
                                      Container(
                                        padding:
                                            EdgeInsets.symmetric(vertical: 20),
                                        child: Text(
                                          LocaleKeys.socialNetworks.tr(),
                                          style: TextStyle(
                                            color: AppColor.white,
                                            fontFamily: AppFont.robotoRegular,
                                            fontSize: 14,
                                          ),
                                        ),
                                      ),
                                      Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          IconButton(
                                            icon: SvgPicture.asset(
                                                Images.facebook),
                                            onPressed: () {},
                                          ),
                                          const SizedBox(width: 13),
                                          IconButton(
                                            icon:
                                                SvgPicture.asset(Images.google),
                                            onPressed: () {},
                                          ),
                                          const SizedBox(width: 13),
                                          IconButton(
                                            icon: SvgPicture.asset(
                                              Images.wiki,
                                            ),
                                            onPressed: () {},
                                          ),
                                        ],
                                      )
                                    ],
                                  ),
                                ))
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          );
        }),
      ),
    );
  }
}
