import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:uz_airways/constants/colors.dart';
import 'package:uz_airways/constants/images.dart';
import 'package:uz_airways/pages/account/profile.dart';
import 'package:uz_airways/pages/airTicket/air_ticket.dart';
import 'package:uz_airways/pages/getTicket/getTicket.dart';

class TabsScreen extends StatefulWidget {
  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  final List<Map<String, dynamic>> _pages = [
    {
      'page': AirTicketScreen(),
      'title': 'Categories',
    },
    {
      'page': GetTicketScreen(),
      'title': 'Your Favorite',
    },
    {
      'page': ProfileScreen(),
      'title': 'Your Favorite',
    }
  ];

  int _selectedPageIndex = 0;
  void _selectPage(int index) {
    setState(() {
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        initialIndex: 0,
        child: Scaffold(
            appBar: null,
            // AppBar(
            //   title: Text(_pages[_selectedPageIndex]['title']),
            //     bottom: TabBar(
            //     tabs: <Widget>[
            //       Tab(
            //         icon: Icon(Icons.category),
            //         text: 'Categories',
            //       ),
            //       Tab(
            //         icon: Icon(Icons.star),
            //         text: 'Favorites',
            //       ),
            //     ],
            //   ),
            // ),
            // drawer: MainDrawer(),
            body: _pages[_selectedPageIndex]['page'],
            // TabBarView(
            //   children: <Widget>[
            //     CategoriesScreen(),
            //     FavoritesScreen(),
            //   ],
            // ),
            bottomNavigationBar: BottomNavigationBar(
              onTap: _selectPage,
              backgroundColor: Theme.of(context).primaryColor,
              unselectedItemColor: AppColor.coolGrey,
              selectedItemColor: AppColor.white,
              currentIndex: _selectedPageIndex,
              type: BottomNavigationBarType.fixed,
              items: [
                BottomNavigationBarItem(
                  backgroundColor: Theme.of(context).primaryColor,
                  // icon: const Icon(Icons.airplanemode_active),
                  icon: SvgPicture.asset(
                    Images.airPlane,
                    color: _selectedPageIndex == 0
                        ? AppColor.white
                        : AppColor.coolGrey,
                  ),
                  label: 'Авиабилеты',
                ),
                BottomNavigationBarItem(
                  backgroundColor: Theme.of(context).primaryColor,
                  // icon: Icon(Icons.airplanemode_active_outlined),
                  icon: SvgPicture.asset(
                    Images.ticket,
                    color: _selectedPageIndex == 1
                        ? AppColor.white
                        : AppColor.coolGrey,
                  ),

                  label: 'Регистрация',
                ),
                BottomNavigationBarItem(
                  backgroundColor: Theme.of(context).primaryColor,
                  // icon: Icon(Icons.person_outline),
                  icon: SvgPicture.asset(
                    Images.account,
                    color: _selectedPageIndex == 2
                        ? AppColor.white
                        : AppColor.coolGrey,
                  ),

                  label: 'Аккаунт',
                ),
              ],
            )));
  }
}
