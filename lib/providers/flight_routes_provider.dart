import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:uz_airways/models/flight_routes_model.dart';
import 'package:uz_airways/utils/shared_pref.dart';

class FlightRoutesProvider with ChangeNotifier {
  FlightRoutes _data = FlightRoutes(success: false, data: []);

  // FlightRoutesProvider() {
  //   initialState();
  // }

  // void initialState() {
  //   syncDataWithProvider();
  // }

  FlightRoutes get data {
    return _data;
  }

  void changeRoutesData(FlightRoutes values) {
    _data = values;
    // SharedPref sharedPref = SharedPref();
    // sharedPref.save('flightRoutes', 'Shaxboz');
    // updateSharedPrefrences();
    notifyListeners();
  }

  // Future updateSharedPrefrences() async {
  //   // List<String> myCards = data.data.map((f) => json.encode(f.toJson())).toList();
  //   SharedPreferences prefs = await SharedPreferences.getInstance();

  //   await prefs.setString('flightRoutes', json.encode(_data));
  // }

  // Future syncDataWithProvider() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   var result = prefs.getString('flightRoutes');

  //   if (result != null) {
  //     _data = json.decode(result);
  //   }
  //   notifyListeners();
  // }
}
