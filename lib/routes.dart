import 'package:flutter/material.dart';
import 'package:uz_airways/pages/airTicket/flight_list.dart';
import 'package:uz_airways/pages/auth/login/login_page.dart';
import 'package:uz_airways/pages/navigators/tabs_screen.dart';
import './pages/auth/recoveryPassword/recovery_password_page.dart';
import './pages/auth/signUp/sign_up_page.dart';
// import 'package:flutter_boilerplate/pages/home/home_page.dart';
// import 'package:flutter_boilerplate/pages/post_detail/post_detail_page.dart';

class Routes {
  Routes._();

  //static variables
  static const String tabs = 'tabs';
  // ignore: constant_identifier_names
  static const String loginPage = LoginPage.routeName;
  static const String signUp = SignUpPage.routeName;
  static const String recoveryPass = RecoveryPasswordPage.routeName;
  static const String flightList = FlightList.routeName;

  static final routes = <String, WidgetBuilder>{
    tabs: (BuildContext context) => TabsScreen(),
    loginPage: (BuildContext context) => LoginPage(),
    signUp: (BuildContext context) => SignUpPage(),
    recoveryPass: (BuildContext context) => RecoveryPasswordPage(),
    flightList: (BuildContext context) => FlightList(),
  };
}
