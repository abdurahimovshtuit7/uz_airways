// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> ru = {
  "enter": "Войти",
  "password": "Пароль",
  "emailPhoneNumber": "Адрес эл. почты или телефона",
  "forgotPassword": "Забыли пароль?",
  "haveAccount": "У меня есть аккаунт",
  "registorNow": "Зарегистрироваться",
  "signUp": "Регистрация",
  "socialNetworks": "Войти с помощью соц. сетей"
};
static const Map<String,dynamic> en = {
  "enter": "Sign In",
  "password": "Password",
  "emailPhoneNumber": "Email address mail or phone",
  "forgotPassword": "Forgot your password?",
  "haveAccount": "I have an account",
  "registorNow": "Registor Now",
  "signUp": "Sign Up",
  "socialNetworks": "Login with social networks"
};
static const Map<String,dynamic> uz = {
  "enter": "Kirish",
  "password": "Parol",
  "emailPhoneNumber": "E-pochta manzili pochta yoki telefon",
  "forgotPassword": "Parolni unutdingizmi?",
  "haveAccount": "Mening profilim bor",
  "registorNow": "Hozir ro'yxatdan o'ting",
  "signUp": "Ro'yxatdan o'tish",
  "socialNetworks": "Ijtimoiy tarmoqlar bilan kirish"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"ru": ru, "en": en, "uz": uz};
}
