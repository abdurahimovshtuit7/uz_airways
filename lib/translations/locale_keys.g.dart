// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const enter = 'enter';
  static const password = 'password';
  static const emailPhoneNumber = 'emailPhoneNumber';
  static const forgotPassword = 'forgotPassword';
  static const haveAccount = 'haveAccount';
  static const registorNow = 'registorNow';
  static const signUp = 'signUp';
  static const socialNetworks = 'socialNetworks';

}
